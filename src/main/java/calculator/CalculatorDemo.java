/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package calculator;

/**
 *
 * @author iuabd
 */
public class CalculatorDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        CalculatorAdd add = new CalculatorAdd(1, 2);
       
        System.out.println("Addition of "+add.getOperand1()+" and "+ add.getOperand2()+ " will result in "+add.add());

        CalculatorSubtract sub = new CalculatorSubtract(1, 2);
        
        System.out.println("Subtraction of "+sub.getOperand1()+" and "+ sub.getOperand2()+ " will result in "+sub.subtract());

    }
}
