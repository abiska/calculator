/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package calculator;

/**
 *
 * @author iuabd
 */
public class CalculatorSubtract extends Calculator{

    public CalculatorSubtract(double operand1, double operand2) {
        super(operand1, operand2);
    }
    
    //getter(s)
    public double getOperand1() {
        return operand1;
    }

    public double getOperand2() {
        return operand2;
    }
    
    //method(s)
    public double subtract(){
        return operand1 - operand2;
    }  
}
